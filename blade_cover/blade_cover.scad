$fn=50;

height=17;
length=72;
thickness = 2;
angle=33;

bumper_depth=1;
bumper_height = 0.6;

border = 1;

angle_offset = height/tan(90-angle);

difference(){
    minkowski(){
        base();
        sphere(border*2);
    }
    translate([0, -border*2,0]){
        cube([thickness, length, height], center = false);
    }
}
bumper();

module base() {
    difference() {
        cube([thickness, length, height], center = false);
        rotate([90,0,90]) {
            linear_extrude(height = 10, center = false, convexity = 10 ) {
                polygon([[0,0],[0,height],[angle_offset, height]]);
            }
        }
    }
}

module bumper() {
    translate([-border, length*0.4, 0]) {
        rotate ([90,180,0]) {
            difference() {
                union() {
                    rotate([90,0,0]) cylinder(h=height * bumper_height, r=border + bumper_depth);
                    translate([0,-height * bumper_height,0]) sphere(r=border + bumper_depth);
                }
                translate([25,0,0]) cube(50, center=true);
            }
        }
    }
}
